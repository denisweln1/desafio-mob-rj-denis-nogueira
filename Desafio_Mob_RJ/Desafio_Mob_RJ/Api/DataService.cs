﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Desafio_Mob_RJ.Models;
using Newtonsoft.Json;

namespace Desafio_Mob_RJ.Api
{
    public static class DataService
    {
       
        /// <summary>
        /// Busca os dados de todos os Estados na Api
        /// </summary>
        /// <returns></returns>
        public static async Task<RootObject> GetEstadosAsync()
        {
            HttpClient client = new HttpClient();
            var response = await client.GetStringAsync("https://api.airtable.com/v0/app0RCW0xYP8RT3U9/Estados?api_key=keyhS9s7U3bGKSuml");
            var rootObject = JsonConvert.DeserializeObject<RootObject>(response);
            return rootObject;
        }

        public static RootObject GetEstados()
        {
            HttpClient client = new HttpClient();
            var response = client.GetStringAsync("https://api.airtable.com/v0/app0RCW0xYP8RT3U9/Estados?api_key=keyhS9s7U3bGKSuml").Result;
            var rootObject = JsonConvert.DeserializeObject<RootObject>(response);
            return rootObject;
        }
    }
}
