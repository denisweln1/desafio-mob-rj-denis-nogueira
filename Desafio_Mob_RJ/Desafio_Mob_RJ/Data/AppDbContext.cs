﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using SQLitePCL;
using Desafio_Mob_RJ.Models;
using Microsoft.EntityFrameworkCore;
using Environment = System.Environment;
namespace Desafio_Mob_RJ.Data
{

    public class AppDbContext : DbContext
    {
        public DbSet<Attachment> Attachment { get; set; }

        public DbSet<Fields> Fields { get; set; }

        public DbSet<Full> Full { get; set; }

        public DbSet<Large> Large { get; set; }

        public DbSet<Record> Record { get; set; }

        

        public DbSet<Thumbnails> Thumbnails { get; set; }
               

        public AppDbContext()
        {
            SQLitePCL.Batteries.Init();
            SQLitePCL.raw.SetProvider(new SQLitePCL.SQLite3Provider_e_sqlite3());

            // var OptionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            // OptionsBuilder.UseSqlite(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "banco.db"));
            //base.OnConfiguring(OptionsBuilder);
            //  _databasePath = databasePath;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //base.OnConfiguring(optionsBuilder);
            ////this.Database.Migrate();
            var dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "banco.db");

            optionsBuilder.UseSqlite($"Filename={dbPath}");

        }


        //protected override void OnModelCreating(ModelBuilder builder)
        //{
        //    base.OnModelCreating(builder);

        //}
    }
}
