﻿using Desafio_Mob_RJ.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Desafio_Mob_RJ
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EstadosBanco : ContentPage
	{
		public EstadosBanco ()
		{
			InitializeComponent ();
            GetTodosEstadosDB();

        }

        private async void GetTodosEstadosDB()
        {
            
            AppDbContext dbContext = new AppDbContext();
            var Estados = await dbContext.Record.Where(r => r.id != "")
                .Include("fields")
                .ToListAsync();

            if (Estados != null  && Estados.Count > 0)
            {
                EstadosList.ItemsSource = Estados;
                LblEstados.Text = "Estados Carregados do Banco de Dados";
            }
            else
            {
                LblEstados.Text = "Não existem Estados gravados no Banco de Dados";
            }
          
        }
    }
}