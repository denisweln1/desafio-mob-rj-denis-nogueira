﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Desafio_Mob_RJ.Models;
using Desafio_Mob_RJ.Api;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Connectivity;

namespace Desafio_Mob_RJ
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EstadosPorRegiao : ContentPage
	{
		public EstadosPorRegiao ()
		{
			InitializeComponent ();

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            GetTodosEstadosPorRegiao();



        }

        private async void GetTodosEstadosPorRegiao()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var Estados = await DataService.GetEstadosAsync();
            var EstadosRegiao = Estados.records;
            EstadosList.ItemsSource = EstadosRegiao;
            }
            else
            {
                await DisplayAlert("Sem Conexao", "Sem conexao com a internet para baixar os dados", "Fechar");
                LblEstados.Text = "Erro ao baixar os dados dos Estados";
            }
        }

       

        private async void PckEstados_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var Estados = await DataService.GetEstadosAsync();

                //EstadosNorteList.ItemsSource =
                var EstadosRegiao = Estados.records.Where(Norte => Norte.fields.Regiao == pckEstados.SelectedItem.ToString()).ToList();
                EstadosList.ItemsSource = EstadosRegiao;
            }
            else
            {
                await DisplayAlert("Sem Conexao", "Sem conexao com a internet para baixar os dados", "Fechar");
                LblEstados.Text = "Erro ao baixar os dados dos Estados";

            }
            }
    }
}