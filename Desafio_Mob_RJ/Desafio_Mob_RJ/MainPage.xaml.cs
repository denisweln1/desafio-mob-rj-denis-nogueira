﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Desafio_Mob_RJ.Api;
using Desafio_Mob_RJ.Data;
using Plugin.Connectivity;

namespace Desafio_Mob_RJ
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();


            //var estadosAPi = estados;
           

            btTodosEstados.Clicked += async  (sender, e) =>
            {
                //App.Current.MainPage = new NavigationPage();
               await  Navigation.PushAsync(new TodosEstados());
            };

            
            btEstadosPorRegiao.Clicked += async (sender, e) =>
            {
               await  Navigation.PushAsync(new EstadosPorRegiao());

            };

            btSalvar.Clicked += async (sender, e) =>
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    try
                {
                    AppDbContext dbContext = new AppDbContext();
                    dbContext.Database.EnsureDeleted();
                    dbContext.Database.EnsureCreated();
                    var estados = await DataService.GetEstadosAsync();

                    //Removendo as urls da página
                    for (int i = 0; i < estados.records.Count; i++)
                    {
                        estados.records[i].fields.Attachments = null;
                    }
                      await dbContext.AddRangeAsync(estados.records);
                    var result = await dbContext.SaveChangesAsync();
                    await DisplayAlert("Sucesso", "Estados Salvo com sucesso no banco de dados", "Fechar");
                }
                catch (Exception erroSalvar)
                {
                    await DisplayAlert("Erro", "Erro ao tentar salvar os Estados no banco de dados", "Fechar");
                    Console.WriteLine(erroSalvar.Message);
                }
                }
                else
                {
                    await DisplayAlert("Sem Conexao", "Sem conexao com a internet para baixar os dados", "Fechar");
                   
                }
                //Salvar dados da APi no banco de dados local
                //estados
            };

            btEstadosDB.Clicked += async (sender, e) =>
            {
                await Navigation.PushAsync(new EstadosBanco());

            };


        }

     
        
    }
}
