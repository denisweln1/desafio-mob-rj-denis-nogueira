﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Desafio_Mob_RJ.Models
{
    public class Attachment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string url { get; set; }
        public string filename { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public Thumbnails thumbnails { get; set; }
    }
}
