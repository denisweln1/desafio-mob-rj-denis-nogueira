﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio_Mob_RJ.Models
{
    public class Record
    {
        public string id { get; set; }
        public Fields fields { get; set; }
        public DateTime createdTime { get; set; }
    }
}
