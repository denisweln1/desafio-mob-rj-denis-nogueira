﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Desafio_Mob_RJ.Models
{
    public class Thumbnails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public Small small { get; set; }
        public Large large { get; set; }
        public Full full { get; set; }
    }
}
