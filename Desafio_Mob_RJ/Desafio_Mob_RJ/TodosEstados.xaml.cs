﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Desafio_Mob_RJ.Models;
using Desafio_Mob_RJ.Api;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Connectivity;

namespace Desafio_Mob_RJ
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TodosEstados : ContentPage
	{
        private List<Record> RecordList;

        public TodosEstados()
		{
			InitializeComponent();


           

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            
                 GetTodosEstados();
            
           

        }
        


        private async void GetTodosEstados()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var Estados = await DataService.GetEstadosAsync();
                EstadosList.ItemsSource = Estados.records;
                LblEstados.Text = "Estados Carregados";
                RecordList = Estados.records;
            }
            else
            {
                await DisplayAlert("Sem Conexao", "Sem conexao com a internet para baixar os dados", "Fechar");
                LblEstados.Text = "Erro ao baixar os dados dos Estados";
            }
        }


        private void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var texto = BuscaEstado.Text;
            if(texto.Length > 0)
            {
                EstadosList.ItemsSource = RecordList.Where(
                 x => x.fields.Estado.ToLower().Contains(texto.ToLower()));
            }
            else
            {
                EstadosList.ItemsSource = RecordList;
            }
           

        }

    }
}